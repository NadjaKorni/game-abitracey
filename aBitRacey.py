#################################################################
# To start the game you need to add:
#line 17: the path to your .wav sound example
#line 18: the path to your .mp3 sound example
#line 38: the path to your .img which will be your character in the game
#line 42: the path to your .img which will be the window icon (665x648 Pixel), not necessary

#################################################################
from typing import List, Any

import pygame
import time
import random

pygame.init()

#################################################################
# SOUNDS:
crash_sound = pygame.mixer.Sound("pump_exp.wav")
pygame.mixer.music.load("bensound-allthat.mp3")
#################################################################
# GLOBAL ONES:

display_width = 800
display_height = 600

black = (0, 0, 0)
white = (255, 255, 255)
red = (255, 0, 0)
bright_red = (200, 0, 0)
green = (0, 255, 0)
bright_green = (0, 200, 0)

ghost_width = 180

gameDisplay = pygame.display.set_mode((display_width, display_height))
pygame.display.set_caption('A bit Racey!')
clock = pygame.time.Clock()

img = pygame.image.load('C:/python36/ghost.png')
img = pygame.transform.scale(img, (300, 150))

# ImgIcon has 665x648 Pixel via settings in Paint.
imgIcon = pygame.image.load('C:/python36/ghostIcon.png')
pygame.display.set_icon(imgIcon)

pause = False

########################################################################
# FUNCTIONS:
def things_dodged(count):
    font = pygame.font.SysFont('comicsansms', 25)
    text = font.render("Dodged: "+str(count), True, black)
    gameDisplay.blit(text, (0, 0))


def things(thingx, thingy, thingw, thingh, color):
    pygame.draw.rect(gameDisplay, color, [thingx, thingy, thingw, thingh])


def ghost (x, y):
    gameDisplay.blit(img, (x, y))


def text_objects(text, font):
    textSurf = font.render(text, True, black)
    return textSurf, textSurf.get_rect()


def message_display(text):
    largeText = pygame.font.SysFont('comicsansms', 115)
    TextSurf, TextRect = text_objects(text, largeText)
    TextRect.center = ((display_width/2), (display_height/2))
    gameDisplay.blit(TextSurf, TextRect)

    pygame.display.update()

    time.sleep(2)

    game_loop()


def crash():
    pygame.mixer.music.stop()
    message_display('You Crashed')
    crash_sound.play()    # isn't working

def button(msg, x, y, w, h, ic, ac, action = None): # action has default None
    mouse = pygame.mouse.get_pos()
    click = pygame.mouse.get_pressed() #click is a list like (1,0,0) for a left click
    #print(click)

    # draw button stuff as useless blobs of color
    if x + w > mouse[0] > x and y + h > mouse[1] > y:
        pygame.draw.rect(gameDisplay, ic, (x, y, w, h))
        if click[0] == 1 and action != None:
            action()
    else:
        pygame.draw.rect(gameDisplay, ac, (x, y, w, h))

    smallText = pygame.font.SysFont('comicsansms', 20)
    textSurf, textRect = text_objects(msg, smallText)
    # we center our text object by hard coding
    textRect.center = ((x + (w / 2)), (y + (h / 2)))
    gameDisplay.blit(textSurf, textRect)

def quit_game():
    pygame.quit()
    quit()

def game_intro():
    intro = True

    while intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()               # uninitialize pygame
                quit()                      # quits game

        gameDisplay.fill(white)

        # hard coded  text in intro
        largeText = pygame.font.SysFont('comicsansms',115)
        TextSurf, TextRect = text_objects("A bit Racey", largeText)
        TextRect.center = ((display_width/2),(display_height/2))
        gameDisplay.blit(TextSurf, TextRect)

        # dynamic coded buttons in intro
        button("GO!", 150, 450, 100, 50, green, bright_green, game_loop)
        button("Stop!", 550, 450, 100, 50, red, bright_red, quit_game)

        pygame.display.update()
        clock.tick(15)


def unpause():
    global pause
    pygame.mixer.music.unpause()
    pause = False


# basically same code like intro but the difficulty is that we want to continue the game, if we press 'continue',
# rather than restart the game_loop/ game_loop(); that's why we created unpause()
def paused():
    pygame.mixer.music.pause()
    while pause:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()               # uninitialize pygame
                quit()                      # quits game

        gameDisplay.fill(white)

        # hard coded  text in intro
        largeText = pygame.font.SysFont('comicsansms',115)
        TextSurf, TextRect = text_objects("Paused", largeText)
        TextRect.center = ((display_width/2),(display_height/2))
        gameDisplay.blit(TextSurf, TextRect)

        # dynamic coded buttons in intro
        button("Continue", 150, 450, 100, 50, green, bright_green, unpause)
        button("Stop!", 550, 450, 100, 50, red, bright_red, quit_game)

        pygame.display.update()
        clock.tick(15)


###############################################################################################
#EVENT HANDELING LOOP
def game_loop():
    x = (display_width * 0.3)
    y = (display_height * 0.7)

    x_change = 0
    ghost_speed = 0

    #creates endless loop for music:
    pygame.mixer.music.play(-1)
    global pause

    thing_startx = random.randrange(0, display_width)
    thing_starty = -600
    thing_speed = 7
    thing_width = 100
    thing_height = 100
    ghost_positionx_rigth = x + 150
    ghost_positionx_left = x + 108
    thing_bottomline = list(range(thing_startx, (thing_startx + thing_width)))

    thingCount = 1

    dodged = 0

#DYNAMIC PART OF THE LOOP:
    gameExit = False
    while not gameExit:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x_change = -5
                if event.key == pygame.K_RIGHT:
                    x_change = 5
                if event.key == pygame.K_p:
                    pause = True
                    paused()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    x_change = 0

        x += x_change

        gameDisplay.fill(white)

############### testing objects
        #pygame.draw.rect(gameDisplay, black, [240, 420, 5, 5])
        #pygame.draw.rect(gameDisplay, black, [ghost_positionx_left, (y+40), 5, 5])
        #pygame.draw.rect(gameDisplay, black, [ghost_positionx_rigth, (y+40), 5, 5])
###############

        #creates things and a int-list of the bottomline of that actual thing
        things(thing_startx, thing_starty, thing_width, thing_height, green)
        ghost_positionx_rigth = x + 150
        ghost_positionx_left = x + 108
        thing_bottomline = list(range(int(thing_startx), int(thing_startx + thing_width)))
        # the block goes because we add thing_speed which is 7 every round of our loop and rescratch our display
        # nothing is really moving, it's a illusion
        thing_starty += thing_speed
        ghost(x, y)
        things_dodged(dodged)

        # -85 because our ghost isn't in the middle of the background
        if x > (display_width - ghost_width) or x < -85:
            crash()

        # asks for a crash between ghost and widget
        if ((thing_starty+thing_height) > (y+40)) and ghost_positionx_rigth in thing_bottomline:
                crash()
        if ((thing_starty + thing_height) > (y + 40)) and ghost_positionx_left in thing_bottomline:
                crash()

        # asks for a crash between ghost and window boarder
        # creates score system, an increasing thing_speed and thing_width
        if thing_starty > display_height:
            thing_starty = 0 - thing_height
            thing_startx = random.randrange(0, display_width)
            dodged += 1
            thing_speed += 1
            thing_width += (dodged * 1.2)

        pygame.display.update()                             # or pygame.display.flip()
        clock.tick(30)                                      # frames per second

game_intro()
game_loop()
pygame.quit()                                           # uninitialize pygame
quit()